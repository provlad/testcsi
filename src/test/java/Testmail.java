import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Testmail {
    private static ChromeDriver driver;
    String currentUrl;
    String url = "https://mail.ru/";

    @Test
    public void s01_BrowserLaunch() {
        System.setProperty("webdriver.chrome.driver", "src/ChromeDriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("start-maximized");
        driver = new ChromeDriver(chromeOptions);
        driver.get(url);
        String currentUrl = driver.getCurrentUrl();

        if (currentUrl.equals(url)) {
            System.out.println("Step01 PASSED");
        } else {
            System.out.println("Step01 FAILED");
        }
    }

    @Test
    public void s02_Login() {
//      Login
        WebElement login = driver.findElementById("mailbox__login");
        login.sendKeys("testcsi");
        WebElement pass = driver.findElementById("mailbox__password");
        pass.sendKeys("123456qW");
        WebElement submit = driver.findElementById("mailbox__auth__button");
        submit.click();

        if (ExpectedConditions.elementToBeClickable(By.className("b-datalist__item__wrapper")) == null) {
            System.out.println("Step02 FAILED");
        } else {
            System.out.println("Step02 PASSED");
        }
    }

    @Test
    public void s03_OpenMail() {
//      Login
        try {
            WebElement pageloaded = (new WebDriverWait(driver, 5)).until(ExpectedConditions.elementToBeClickable(By.className("b-datalist__item__wrapper")));
        } catch (Exception e) {
        }
        try {
            WebElement mail = driver.findElementByClassName("b-datalist__item__wrapper");
            mail.click();
        } catch (Exception e) {
        }

        if (ExpectedConditions.elementToBeClickable(By.className("b-letter__body")) == null) {
            System.out.println("Step03 FAILED");
        } else {
            System.out.println("Step03 PASSED");
        }
    }

    @Test
    public void s04_CheckMail() {
//      Login
        String senderemail, sendername, subjsender, bodyemail;
        try {
            WebElement pageloaded = (new WebDriverWait(driver, 5)).until(ExpectedConditions.elementToBeClickable(By.className("b-letter__body")));
        } catch (Exception e) {
        }
        subjsender = driver.findElement(By.className("b-letter__head__subj__text")).getText();
        senderemail = driver.findElement(By.className("b-letter__head__addrs__from")).getText();
        bodyemail = driver.findElement(By.className("b-letter__body")).getText();

        if ((subjsender.equals("test")) && (bodyemail.equals("Test")) && (senderemail.equals("Тест Тестов <testcsi@mail.ru>"))) {
            System.out.println("Step04 PASSED");
        } else {
            System.out.println("Step04 FAILED");
        }
    }
}
